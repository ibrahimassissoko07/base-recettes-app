import React, { Component } from 'react'
import PropTypes from 'prop-types';
// CSS
import './App.css'
import Admin from './components/Admin'
import Header from './components/Header'
import Card from './components/Card'
import withFirebase from './hoc/withFirebase'
import  ColorContext  from './components/Color';

class App extends Component {
  render() {
    const Cards = Object.keys(this.props.recettes)
      .map(key => <Card key={key} details={this.props.recettes[key]} />)
    return (
      <ColorContext>
        <div className='box'>
          <Header pseudo={this.props.pseudo} />
          <div className='cards'>
            {Cards}
          </div>
          <Admin 
            pseudo = {this.props.pseudo}
            recettes = {this.props.recettes}
            supprimerRecette = {this.props.supprimerRecette}
            majRecette = {this.props.majRecette}
            ajouterRecette = {this.props.ajouterRecette}
            handleChargerExemple={this.props.handleChargerExemple} />
        </div>        
      </ColorContext>
    )
  }
}

App.PropTypes = {
  pseudo: PropTypes.string.isRequired,
  recettes: PropTypes.object.isRequired,
  supprimerRecette: PropTypes.func.isRequired,
  majRecette: PropTypes.func.isRequired,
  ajouterRecette: PropTypes.func.isRequired,
  handleChargerExemple: PropTypes.func.isRequired,
}
const WrappedComponent = withFirebase(App)
export default WrappedComponent;
