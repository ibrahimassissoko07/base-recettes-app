import React, { Component } from 'react'
import recettes from '../recettes';
import base from '../base';

const withFirebase = WrappedComponent => (
    class HOC extends Component{
        state = {
            pseudo: this.props.match.params.pseudo,
            recettes: {}
          }
          componentDidMount() {
            this.ref = base.syncState(`/${this.state.pseudo}/recettes`, {
              context: this,
              state: 'recettes'
            })
          }
          componentWillUnmount() {
            base.removeBinding(this.ref)
          }
          handleChargerExemple = () => {
            this.setState({ recettes });
          }
        
          supprimerRecette = (key) =>{
            const recettes = {...this.state.recettes};
            recettes[key] = null;
            this.setState({ recettes });
          }
          majRecette = (key, newRecette) =>{
            const recettes = { ...this.state.recettes};
            recettes[key] = newRecette;
            this.setState({recettes});
          }
          ajouterRecette = recettte => {
            const recettes = { ...this.state.recettes };
            recettes[`recette-${Date.now()}`] = recettte;
            this.setState({ recettes });
          }
        render(){
            return(
                <WrappedComponent
                    recettes = {this.state.recettes}
                    pseudo = {this.state.pseudo}
                    handleChargerExemple={this.handleChargerExemple}
                    supprimerRecette={this.supprimerRecette}
                    majRecette={this.majRecette}
                    ajouterRecette={this.ajouterRecette}
                    {...this.props}
                />
            )
        }
    }
)

export default withFirebase