import Rebase  from "re-base";
import firebase from "firebase/app";
import 'firebase/database';

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyBsU4ch5JnHxVEXCELUizI51yFXB8N8kTg",
  authDomain: "base-recettes-app-77ecc.firebaseapp.com",
  databaseURL: "https://base-recettes-app-77ecc-default-rtdb.firebaseio.com",
})

const base = Rebase.createClass(firebaseApp.database());

export { firebaseApp };

export default base;
