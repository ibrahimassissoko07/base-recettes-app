import React, { Component } from 'react'
import AdminForm from './AdminForm'
import AjouterRecette from './AjouterRecette'
import Login from './Login'
import firebase from 'firebase/app'
import 'firebase/auth'
import base, {firebaseApp} from '../base';

export default class Admin extends Component {
  state = {
    uid:null,
    chef:null
  }

  componentDidMount(){
    firebase.auth().onAuthStateChanged(user =>{
      if (user) {
        this.handleAuth({user});
      }
    })
  }
  handleAuth = async authData =>{
    const box = await base.fetch(this.props.pseudo, {context: this});

    if (!box.chef) {
      await base.post(`${this.props.pseudo}/chef`, {
        data: authData.user.uid
      })
    }

    this.setState({
      uid: authData.user.uid,
      chef: box.chef || authData.user.uid
    })
    
  }
  authenticate = ()=>{
    const authProvider = new firebase.auth.GoogleAuthProvider();
    firebaseApp
      .auth()
      .signInWithPopup(authProvider)
      .then(this.handleAuth)
  }

  logout = async  ()=>{
    console.log("Déconnexion");
    await firebase.auth().signOut();
    this.setState({ uid : null});
  }
  render() {

    const logout = <button onClick={this.logout}>Deconexion</button>
    if (!this.state.uid) {
      return <Login authenticate={this.authenticate}/>
    }
    if (this.state.uid ===! this.state.chef) {
      return(
        <div>
          <p>Tu n'est pas le chef de cette boîte</p>
          {logout}
        </div>
      )
    }
    const {ajouterRecette, handleChargerExemple, recettes, majRecette, supprimerRecette} = this.props
    return (
      <div className='cards'>
        <AjouterRecette ajouterRecette = {ajouterRecette}/>
        {
          Object.keys(recettes).map(key => <AdminForm
            key={key}
            id={key}
            majRecette={majRecette}
            supprimerRecette={supprimerRecette}
            recettes = {recettes}
          />)
        }
        <footer>
          <button onClick={handleChargerExemple}>Remplir</button>
          {logout}
        </footer>
      </div>
    )
  }
}
