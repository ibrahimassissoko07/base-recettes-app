import React from 'react'

const AdminForm = ({id: key,
  majRecette,
  recettes, supprimerRecette}) => {
    const handleChange = (e, key) =>{
      const {name, value} = e.target;
      const recette = recettes[key];
      recette[name]=value;
      majRecette(key, recette);
    }
  return (
    <div className='card'>

        <form className='admin-form'>
            <input value={recettes[key].nom} type='text' name='nom' onChange={e => handleChange(e, key)} placeholder='Nom de la recette'/>
            <input value={recettes[key].image} type='text' name='image' onChange={e => handleChange(e, key)} placeholder="L'adresse de l'image du recette"/>
            <textarea value={recettes[key].ingredients} name='ingredients' onChange={e => handleChange(e, key)} placeholder='Liste des ingredients' rows='3'/>
            <textarea value={recettes[key].instructions} name='instructions' onChange={e => handleChange(e, key)} placeholder='Liste des instructions' rows='15'/>
        </form>
        <button onClick={()=>supprimerRecette(key)}>Supprimer</button>
    </div>
  )
}

export default AdminForm