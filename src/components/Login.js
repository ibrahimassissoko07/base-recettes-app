import React from 'react'

const Login = ({authenticate}) => {
  return (
    <div className='login'>
        <h2>Connecte toi pour créer un recette</h2>
        <button onClick={authenticate} className='facebook-button'>
            Me Connecter avec Google
        </button>
    </div>
  )
}

export default Login